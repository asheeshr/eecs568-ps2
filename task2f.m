%Over and under estimating motion noise

filter_no = [1 2 3];
ekf_localization_error = [];
ukf_localization_error = [];
pf_localization_error = [];
alphas = [0.001 0.005 0.01 0.05 0.25 0.50]%, 10e-1, 1:1:4];
iterations = 60;
trials = 3;
counter = 0;

for i=alphas
    counter = counter + 1;
    disp('Running for alpha = ')
    disp(i)
    trial_data = zeros(3,trials);
    for j=1:trials
        [data, results] = runfunc(iterations,10e-6,false,deg2rad(20),filter_no,100,[i minimizedAngle(i/100) i minimizedAngle(i/100)].^2);
        trial_data(1,j) = mean(sqrt(results(1,1:iterations).^2+results(2,1:iterations).^2));
        trial_data(2,j) = mean(sqrt(results(1,iterations+1:2*iterations).^2+results(2,iterations+1:2*iterations).^2));
        trial_data(3,j) = mean(sqrt(results(1,2*iterations+1:3*iterations).^2+results(2,2*iterations+1:3*iterations).^2));
    end
    ekf_localization_error = [ekf_localization_error; mean(trial_data(1,:)) , 3*std(trial_data(1,:))];
    ukf_localization_error = [ukf_localization_error; mean(trial_data(2,:)) , 3*std(trial_data(2,:))];
    pf_localization_error = [pf_localization_error;   mean(trial_data(3,:)) , 3*std(trial_data(3,:))];    
    
    figure(200)
    clf
    hold on
    % plot(beta,ekf_localization_error,beta,ukf_localization_error,beta,pf_localization_error)
    set(gca,'xscale','log');
    errorbar(alphas(1:counter),ekf_localization_error(:,1),ekf_localization_error(:,2))
    errorbar(alphas(1:counter),ukf_localization_error(:,1),ukf_localization_error(:,2))
    errorbar(alphas(1:counter),pf_localization_error(:,1),pf_localization_error(:,2))

    title('Effect of Motion Noise on Filters')
    legend('EKF','UKF','PF')
    xlabel('Alpha1')
    ylabel('Avg Localization Error')

end

filename = strcat('../../../figures/task2e','alphasoverunder');
print(filename,'-dpng');