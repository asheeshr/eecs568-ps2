%Iterating over number of particles

filter_no = [3];
pf_localization_error = [];
nparticles = [];
iterations = 75;
trials = 3;
nparticles = [10:10:50, 50:100:500];
counter = 0;

for i=nparticles
    counter = counter + 1;
    disp('Running for particles = ')
    disp(i)
    trial_data = zeros(1,trials);
    for j=1:trials
        [data, results] = runfunc(iterations,10e-6,false,pi/32,filter_no,i);
        trial_data(1,j) = mean(sqrt(results(1,1:iterations).^2+results(2,1:iterations).^2));
    end
    pf_localization_error = [pf_localization_error;   mean(trial_data(1,:)) , 3*std(trial_data(1,:))];    
    
    figure(200)
    clf
    hold on
    set(gca,'xscale','log');

    %plot(beta,ekf_localization_error,beta,ukf_localization_error,beta,pf_localization_error)
    errorbar(nparticles(1:counter),pf_localization_error(:,1),pf_localization_error(:,2));

    title('Effect of Number of Particle')
    legend('PF')
    xlabel('Particles')
    ylabel('Avg Localization Error')

end

filename = strcat('../../../figures/task2d','nsamples');
print(filename,'-dpng');