%Global Localization Test

filter_no = [1 2 3];
ekf_localization_error = [];
ukf_localization_error = [];
pf_localization_error = [];
beta = deg2rad(20);
iterations = 200;
trials = 5;
counter=0;
ekf_history = [];
ukf_history = [];
pf_history = [];

for i=1:1:1
    counter = counter+1;
    disp('Running for beta = ')
    disp(i)
    trial_data = zeros(3,trials);
    for j=1:trials
%         initialStateMean = [600*rand(), 400*rand(), minimizedAngle(pi*rand())];
        [data, results] = runfunc(iterations,10e-6,false,beta,filter_no, 100); %[], initialStateMean );
%         trial_data(1,j) = (sqrt(results(1,1:iterations).^2+results(2,1:iterations).^2));
%         trial_data(2,j) = (sqrt(results(1,iterations+1:2*iterations).^2+results(2,iterations+1:2*iterations).^2));
%         trial_data(3,j) = (sqrt(results(1,2*iterations+1:3*iterations).^2+results(2,2*iterations+1:3*iterations).^2));
        ekf_history = [ekf_history; results(1,1:iterations); results(2,1:iterations); results(2,1:iterations)];
        ukf_history = [ukf_history; results(1,iterations+1:2*iterations); results(2,iterations+1:2*iterations); results(3,iterations+1:2*iterations)];
        pf_history = [pf_history; results(1,2*iterations+1:3*iterations); results(2,2*iterations+1:3*iterations); results(3,2*iterations+1:3*iterations)];
    end
%     figure(200)
%     clf
%     hold on
%     
%     for i=1:trials
%     plot(1:iterations, ekf_history())
%     
%     title('Effect of Sensor Noise on Filters')
%     legend('EKF','UKF','PF')
%     xlabel('Beta (rad)')
%     ylabel('Avg Localization Error')
% 
    figure(100)
    clf
    hold on 
    n = length(filter_no);
    color = rand(3*trials+1,3);
    if max(filter_no == 1) %Call EKF
        subplot(n,3,1)
        hold on
        refline(0,0)
        for i=1:3:3*trials
        plot(ekf_history(i,:), 'color', color(i,:));
        end
        title('EKF X Error')

        subplot(n,3,2)
        hold on
        refline(0,0)
        for i=1:3:3*trials
        plot(ekf_history(i+1,:), 'color', color(i,:));
        end
        title('EKF Y Error')

        subplot(n,3,3)
        hold on
        refline(0,0)
        for i=1:3:3*trials
        plot(ekf_history(i+2,:), 'color', color(i,:));
        end
        title('EKF Angle Error')

    end
% 
    if max(filter_no == 2) %Call EKF
        subplot(n,3,4)
        hold on
        refline(0,0)
        for i=1:3:3*trials
        plot(ukf_history(i,:), 'color', color(i,:));
        end
        title('UKF X Error')

        subplot(n,3,5)
        hold on
        refline(0,0)
        for i=1:3:3*trials
        plot(ukf_history(i+1,:), 'color', color(i,:));
        end
        title('UKF Y Error')

        subplot(n,3,6)
        hold on
        refline(0,0)
        for i=1:3:3*trials
        plot(ukf_history(i+2,:), 'color', color(i,:));
        end
        title('UKF Angle Error')

    end
%     

    if max(filter_no == 2) %Call EKF
        subplot(n,3,7)
        hold on
        refline(0,0)
        for i=1:3:3*trials
        plot(pf_history(i,:), 'color', color(i,:));
        end
        title('PF X Error')

        subplot(n,3,8)
        hold on
        refline(0,0)
        for i=1:3:3*trials
        plot(pf_history(i+1,:), 'color', color(i,:));
        end
        title('PF Y Error')

        subplot(n,3,9)
        hold on
        refline(0,0)
        for i=1:3:3*trials
        plot(pf_history(i+2,:), 'color', color(i,:));
        end
        title('PF Angle Error')

    end% end
    
end


filename = strcat('../../../figures/task2h','kidnappedrobot');
print(filename,'-dpng');