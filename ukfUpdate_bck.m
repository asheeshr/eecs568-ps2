function [mu, Sigma, predMu, predSigma, zhat, G, R, H, K ] = ukfUpdate( ...
    mu, Sigma, u, deltaT, z, Q, markerId, ...
    alphas) %Added to compute R matrix)

% NOTE: The header is not set in stone.  You may change it if you like.
global FIELDINFO;
landmark_x = FIELDINFO.MARKER_X_POS(markerId);
landmark_y = FIELDINFO.MARKER_Y_POS(markerId);

stateDim=3;
motionDim=3;
observationDim=2;

% --------------------------------------------
% Setup UKF
% --------------------------------------------
disp('.......................................')

H = 0;
G = 0;

J = [
    -u(2)*sin(mu(3)+u(1)),  cos(mu(3)+u(1)), 0;
    u(2)*cos(mu(3)+u(1)),  sin(mu(3)+u(1)), 0;
    1,                          0,                   1];

sigma_motion = [
    alphas(1)*u(1)^2 + alphas(2)*u(2)^2, 0, 0;
    0, alphas(4)*u(1)^2 + alphas(4)*u(3)^2 + alphas(3)*u(2)^2, 0;
    0, 0, alphas(1)*u(3)^2 + alphas(2)*u(2)^2];
                
R = J'*sigma_motion*J;


% UKF params
ukf_kappa = 0;
ukf_alpha = 1;
ukf_beta = 2; %Fixed for Gaussian
ukf_n = 3;
ukf_m = 3;
ukf_nm = ukf_n + ukf_m; %Augmented state dims
ukf_lambda = ukf_alpha^2*(ukf_nm + ukf_kappa) - ukf_nm;

% Augmented state
mu_augmented = [mu; zeros(size(R)*[1;0],1)]; %Works for squares only
sigma = Sigma;
sigma_augmented = blkdiag(sigma,R);

% Sigma points
sigma_points = [mu_augmented];
sigma_augmented_sqrt = (ukf_nm+ukf_lambda)^0.5*chol(sigma_augmented);
for i=1:ukf_nm
    sigma_points = [sigma_points, mu_augmented + sigma_augmented_sqrt(:,i)];
    sigma_points(3,i+1) = minimizedAngle(sigma_points(3,i+1)); 
    %Check the angle of the i column after the first column which has mean
end

for i=1:ukf_nm
    sigma_points = [sigma_points, mu_augmented - sigma_augmented_sqrt(:,i)];
    sigma_points(3,ukf_nm+i+1) = minimizedAngle(sigma_points(3,ukf_nm+i+1)); 
    %Check the angle of the i column after the first column which has mean
end %Check indices

plot(sigma_points(1,:),sigma_points(2,:),'b')
% Weights
weights_mean = ukf_lambda/(ukf_nm + ukf_lambda);
weights_cov = weights_mean + 1 - ukf_alpha^2 + ukf_beta;

weights_mean = [ weights_mean, repmat((2*(ukf_nm + ukf_lambda))^(-1),1,2*ukf_nm)];
weights_cov = [ weights_cov, repmat((2*(ukf_nm + ukf_lambda))^(-1),1,2*ukf_nm)];
%weights_cov = weights_cov/norm(weights_cov,1);

% --------------------------------------------
% Prediction step
% --------------------------------------------

% UKF prediction of mean and covariance
g_sigma_points = zeros(ukf_nm,2*ukf_nm+1);
predMu = zeros(ukf_nm,1);
predSigma = zeros(ukf_nm, ukf_nm);

%For circular mean
sinsum = 0;
cossum = 0;

for i = 1:2*ukf_nm+1
    g_sigma_points(:,i) = [
        sigma_points(1,i) + u(2)*cos(sigma_points(3,i)+u(1));
        sigma_points(2,i) + u(2)*sin(sigma_points(3,i)+u(1));
        sigma_points(3,i) + u(1) + u(3);
        sigma_points(4,i);
        sigma_points(5,i);
        sigma_points(6,i);
        ]; %Not sure about this. Modify if dimensions change
    g_sigma_points(3,i) = minimizedAngle(g_sigma_points(3,i));
    
    sinsum = sinsum + weights_mean(i)*sin(g_sigma_points(3,i));
    cossum = cossum + weights_mean(i)*cos(g_sigma_points(3,i));
    
    predMu = predMu + weights_mean(i)*g_sigma_points(:,i);
end

predMu(3) = minimizedAngle(atan2(sinsum/sum(weights_mean),cossum/sum(weights_mean)));

for i = 1:2*ukf_nm+1
    predSigma = predSigma + weights_cov(i)*(g_sigma_points(:,i)-predMu)*(g_sigma_points(:,i)-predMu)';
end

%disp(g_sigma_points);
disp('Input'); disp(u);
disp(predMu);
disp(predSigma);

%--------------------------------------------------------------
% Correction step
%--------------------------------------------------------------

% Sigma points
pred_sigma_points = [predMu];
predSigma_augmented_sqrt = (ukf_nm+ukf_lambda)^0.5*chol(predSigma);
for i=1:ukf_nm
    pred_sigma_points = [pred_sigma_points, predMu + predSigma_augmented_sqrt(:,i)];
    pred_sigma_points(3,i+1) = minimizedAngle(pred_sigma_points(3,i+1)); 
    %Check the angle of the i column after the first column which has mean
end

for i=1:ukf_nm
    pred_sigma_points = [pred_sigma_points, predMu - predSigma_augmented_sqrt(:,i)];
    pred_sigma_points(3,ukf_nm+i+1) = minimizedAngle(pred_sigma_points(3,ukf_nm+i+1)); 
    %Check the angle of the i column after the first column which has mean
end %Check indices

plot(pred_sigma_points(1,:),pred_sigma_points(2,:),'b')


% UKF correction of mean and covariance
z_sigma_points = [];
zhat = 0;
zhat_sigma = 0;
x_zhat_sigma = zeros(ukf_nm,1); %Second dim depends on z

%For circular mean
sinsum = 0;
cossum = 0;

for i = 1:2*ukf_nm+1
    disty = landmark_y-pred_sigma_points(2,i);
    distx = landmark_x-pred_sigma_points(1,i);
    z_sigma_points = [z_sigma_points, atan2(disty, distx) - pred_sigma_points(3,i)];
    z_sigma_points(i) = minimizedAngle(z_sigma_points(i)); 
    
    sinsum = sinsum + weights_mean(i)*sin(z_sigma_points(i));
    cossum = cossum + weights_mean(i)*cos(z_sigma_points(i));
    %zhat = zhat + weights_mean(i)*z_sigma_points(i);
end
zhat  = minimizedAngle(atan2(sinsum/sum(weights_mean),cossum/sum(weights_mean)));

for i = 1:2*ukf_nm+1
    zhat_sigma = zhat_sigma + weights_cov(i)*(z_sigma_points(i)-zhat)*(z_sigma_points(i)-zhat)' + Q;
    x_zhat_sigma = x_zhat_sigma + weights_cov(i)*(pred_sigma_points(:,i)-predMu)*(z_sigma_points(i)-zhat)';
end

%disp(zhat * 180 / pi)
%disp(zhat_sigma)

K = x_zhat_sigma/zhat_sigma

mu = predMu + K*(z - zhat)
mu(3) = minimizedAngle(mu(3));
Sigma = predSigma - K*zhat_sigma*K'