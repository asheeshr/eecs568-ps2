function [mu, Sigma, predMu, predSigma, zhat, G, R, H, K ] = ekfUpdate( ...
    mu, Sigma, u, deltaT, z, Q, markerId, ...
    alphas) %Added to compute R matrix

% NOTE: The header is not set in stone.  You may change it if you like.
global FIELDINFO;
landmark_x = FIELDINFO.MARKER_X_POS(markerId);
landmark_y = FIELDINFO.MARKER_Y_POS(markerId);

stateDim=3;
motionDim=3;
observationDim=2;

% --------------------------------------------
% Prediction step
% --------------------------------------------
mu = mu;
sigma = Sigma;
% g_old_mu = [
%     u(2)*cos(old_mu(3)+u(1));
%     u(2)*sin(old_mu(3)+u(1));
%     minimizedAngle(u(1) + u(3))]; % Funtion of x_(t-1) and u_(t)

J = [
    -u(2)*sin(mu(3)+u(1)),  cos(mu(3)+u(1)), 0;
    u(2)*cos(mu(3)+u(1)),  sin(mu(3)+u(1)), 0;
    1,                          0,                   1];

sigma_motion = [
    alphas(1)*u(1)^2 + alphas(2)*u(2)^2, 0, 0;
    0, alphas(4)*u(1)^2 + alphas(4)*u(3)^2 + alphas(3)*u(2)^2, 0;
    0, 0, alphas(1)*u(3)^2 + alphas(2)*u(2)^2];
                
R = J'*sigma_motion*J;

G = [
    1, 0, -u(2)*sin(mu(3)+u(1));
    0, 1,  u(2)*cos(mu(3)+u(1));
    0, 0,  1];
 
% EKF prediction of mean and covariance
%mu_bar =  old_mu + g_old_mu;
mu_bar = sampleOdometry(u,mu,alphas); %Handles angle rollover
sigma_bar = G*sigma*G' + R;

predMu = mu_bar;
predSigma = sigma_bar;
%--------------------------------------------------------------
% Correction step
%--------------------------------------------------------------
%disp('...................................')
% Compute expected observation and Jacobian
disty = landmark_y-mu_bar(2);
distx = landmark_x-mu_bar(1);

h_mu_bar = atan2(disty, distx) - mu_bar(3);
h_mu_bar = minimizedAngle(h_mu_bar);

dist = (distx^2 + disty^2)^0.5;
delta = 10^-5;
%H = [disty/dist, -(distx)/dist, -1]
H = [ ...
     (atan2(disty, distx - delta) - atan2(disty, distx))/(delta),...
     (atan2(disty - delta, distx) - atan2(disty, distx))/(delta),...
     -delta/delta];

% Innovation / residual covariance
zhat = z - h_mu_bar;
zhat = minimizedAngle(zhat);

% Kalman gain
K = sigma_bar*H'/(H*sigma_bar*H' + Q);

% Correction
mu = mu_bar + K*(zhat);
mu(3) = minimizedAngle(mu(3));
Sigma = (eye(3) - K*H)*sigma_bar;