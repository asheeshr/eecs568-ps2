function [mu, Sigma, predMu, predSigma, zhat, G, R, H, K ] = ukfUpdate( ...
    mu, Sigma, u, deltaT, z, Q, markerId, ...
    alphas) %Added to compute R matrix)

% NOTE: The header is not set in stone.  You may change it if you like.
global FIELDINFO;
landmark_x = FIELDINFO.MARKER_X_POS(markerId);
landmark_y = FIELDINFO.MARKER_Y_POS(markerId);

stateDim=3;
motionDim=3;
observationDim=2;

% --------------------------------------------
% Setup UKF
% --------------------------------------------
%disp('.......................................')

H = 0;
G = 0;

J = [
    -u(2)*sin(mu(3)+u(1)),  cos(mu(3)+u(1)), 0;
    u(2)*cos(mu(3)+u(1)),  sin(mu(3)+u(1)), 0;
    1,                          0,                   1];

sigma_motion = [
    alphas(1)*u(1)^2 + alphas(2)*u(2)^2, 0, 0;
    0, alphas(4)*u(1)^2 + alphas(4)*u(3)^2 + alphas(3)*u(2)^2, 0;
    0, 0, alphas(1)*u(3)^2 + alphas(2)*u(2)^2];
                
R = J'*sigma_motion*J;


% UKF params
ukf_kappa = 0;
ukf_alpha = 1;
ukf_beta = 2; %Fixed for Gaussian
ukf_n = 3;
ukf_m = 3;
ukf_z = 1;
ukf_nmz = ukf_n + ukf_m + ukf_z; %Augmented state dims
ukf_lambda = ukf_alpha^2*(ukf_nmz + ukf_kappa) - ukf_nmz;

% Augmented state
mu_augmented = [mu; zeros(4,1)];
sigma = Sigma;
sigma_augmented = blkdiag(sigma,sigma_motion,Q);

% Sigma points
sigma_points = [mu_augmented];
sigma_augmented_sqrt = (ukf_nmz+ukf_lambda)^0.5*sqrtm(sigma_augmented);
sigma_points = repmat(mu_augmented, 1, 2*ukf_nmz+1);
sigma_points(:,2:ukf_nmz+1) = sigma_points(:,2:ukf_nmz+1) + sigma_augmented_sqrt;
sigma_points(:,ukf_nmz+2:2*ukf_nmz+1) = sigma_points(:,ukf_nmz+2:2*ukf_nmz+1) - sigma_augmented_sqrt;

% for i=1:ukf_nmz
%     sigma_points = [sigma_points, mu_augmented + sigma_augmented_sqrt(:,i)];
% %      sigma_points(3,i+1) = minimizedAngle(sigma_points(3,i+1));
% %      sigma_points(7,i+1) = minimizedAngle(sigma_points(7,i+1)); 
% %     %Check the angle of the i column after the first column which has mean
% end
% 
% for i=1:ukf_nmz
%     sigma_points = [sigma_points, mu_augmented - sigma_augmented_sqrt(:,i)];
% %      sigma_points(3,ukf_nmz+i+1) = minimizedAngle(sigma_points(3,ukf_nmz+i+1)); 
% %      sigma_points(7,ukf_nmz+i+1) = minimizedAngle(sigma_points(7,ukf_nmz+i+1)); 
%     %Check the angle of the i column after the first column which has mean
% end %Check indices

% scatter(sigma_points(1,:),sigma_points(2,:),'b')
%disp(sigma_points)

% Weights
weights_mean = ukf_lambda/(ukf_nmz + ukf_lambda);
weights_cov = weights_mean + 1 - ukf_alpha^2 + ukf_beta;

weights_mean = [ weights_mean, repmat((2*(ukf_nmz + ukf_lambda))^(-1),1,2*ukf_nmz)];
weights_cov = [ weights_cov, repmat((2*(ukf_nmz + ukf_lambda))^(-1),1,2*ukf_nmz)];

% --------------------------------------------
% Prediction step
% --------------------------------------------

% UKF prediction of mean and covariance
g_sigma_points = zeros(ukf_nmz,2*ukf_nmz+1);
predMu = zeros(ukf_n,1);
predSigma = zeros(ukf_n, ukf_n);

%For circular mean
sinsum = 0;
cossum = 0;

for i = 1:2*ukf_nmz+1
    u_noisy = u + sigma_points(4:6,i);
    u_noisy(1) = minimizedAngle(u_noisy(1));
    u_noisy(3) = minimizedAngle(u_noisy(3));
    g_sigma_points(:,i) = [
        sigma_points(1,i) + u_noisy(2)*cos(sigma_points(3,i)+u_noisy(1));
        sigma_points(2,i) + u_noisy(2)*sin(sigma_points(3,i)+u_noisy(1));
        sigma_points(3,i) + u_noisy(1) + u_noisy(3);
        sigma_points(4,i);
        sigma_points(5,i);
        sigma_points(6,i);
        sigma_points(7,i);
        ]; %Not sure about this. Modify if dimensions change
    g_sigma_points(3,i) = minimizedAngle(g_sigma_points(3,i));
    
    sinsum = sinsum + weights_mean(i)*sin(g_sigma_points(3,i));
    cossum = cossum + weights_mean(i)*cos(g_sigma_points(3,i));
    
    predMu = predMu + weights_mean(i)*g_sigma_points(1:3,i);
end

predMu(3) = atan2(sinsum/sum(weights_mean),cossum/sum(weights_mean));

for i = 1:2*ukf_nmz+1
    g_mu_diff = g_sigma_points(1:3,i)-predMu;
    g_mu_diff(3) = minimizedAngle(g_mu_diff(3));
    predSigma = predSigma + weights_cov(i)*(g_mu_diff)*(g_mu_diff)';
end

%--------------------------------------------------------------
% Correction step
%--------------------------------------------------------------

% UKF correction of mean and covariance
z_sigma_points = [];
zhat = 0;
zhat_sigma = 0;
x_zhat_sigma = zeros(ukf_n,ukf_z);

%For circular mean
sinsum = 0;
cossum = 0;

disty = repmat(landmark_y,1,2*ukf_nmz+1) - g_sigma_points(2,:);
distx = repmat(landmark_x,1,2*ukf_nmz+1) - g_sigma_points(1,:);
z_sigma_points = atan2(disty, distx) - g_sigma_points(3,:) + sigma_points(7,:);


for i = 1:2*ukf_nmz+1
%     disty = landmark_y - g_sigma_points(2,i);
%     distx = landmark_x - g_sigma_points(1,i);
%     z_sigma_points = [z_sigma_points, atan2(disty, distx) - g_sigma_points(3,i) + sigma_points(7,i)];
    z_sigma_points(i) = minimizedAngle(z_sigma_points(i));    
%     sinsum = sinsum + weights_mean(i)*sin(z_sigma_points(i));
%     cossum = cossum + weights_mean(i)*cos(z_sigma_points(i));
%     zhat = zhat + weights_mean(i)*z_sigma_points(i);
end

% zhat = minimizedAngle(zhat);
% zhat  = atan2(sinsum/sum(weights_mean),cossum/sum(weights_mean));
zhat  = atan2(weights_mean*sin(z_sigma_points)'/sum(weights_mean), ...
    weights_mean*cos(z_sigma_points)'/sum(weights_mean));

for i = 1:2*ukf_nmz+1

    z_zhat_diff = z_sigma_points(i)-zhat;
    z_zhat_diff = minimizedAngle(z_zhat_diff);
    
    g_mu_diff = g_sigma_points(1:3,i)-predMu;
    g_mu_diff(3) = minimizedAngle(g_mu_diff(3));

    zhat_sigma = zhat_sigma + weights_cov(i)*(z_zhat_diff)*(z_zhat_diff)';
    x_zhat_sigma = x_zhat_sigma + weights_cov(i)*(g_mu_diff(1:3))*(z_zhat_diff)';
end

K = x_zhat_sigma/zhat_sigma;

mu = predMu + K*(z - zhat);
mu(3) = minimizedAngle(mu(3));
Sigma = predSigma - K*zhat_sigma*K';