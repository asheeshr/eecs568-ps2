function varargout = run(stepsOrData, pauseLen, makeVideo, beta, filter_no, pf_numSamples, alphas, initialStateMean )
% RUN  PS2 Feature-Based Localization Simulator
%   RUN(ARG)
%   RUN(ARG, PAUSLEN, MAKEVIDEO)
%      ARG - is either the number of time steps, (e.g. 100 is a complete
%            circuit) or a data array from a previous run.
%      PAUSELEN - set to `inf`, to manually pause, o/w # of seconds to wait
%                 (e.g., 0.3 is the default)
%      MAKEVIDEO - boolean specifying whether to record a video or not
%
%   DATA = RUN(ARG,PAUSELEN)
%      DATA - is an optional output and contains the data array generated
%             and/or used during the simulation.

%   (c) 2009-2015
%   Ryan M. Eustice
%   University of Michigan
%   eustice@umich.edu

if ~exist('pauseLen','var') || isempty(pauseLen)
    pauseLen = 0.3; % seconds
end
if ~exist('makeVideo','var') || isempty(makeVideo)
    makeVideo = false;
end

%--------------------------------------------------------------
% Graphics
%--------------------------------------------------------------

NOISEFREE_PATH_COL = 'green';
ACTUAL_PATH_COL = 'blue';

NOISEFREE_BEARING_COLOR = 'cyan';
OBSERVED_BEARING_COLOR = 'red';

GLOBAL_FIGURE = 1;

if makeVideo
    try
        votype = 'avifile';
        vo = avifile('video.avi', 'fps', min(5, 1/pauseLen));
    catch
        votype = 'VideoWriter';
        vo = VideoWriter('video', 'MPEG-4');
        set(vo, 'FrameRate', min(5, 1/pauseLen));
        open(vo);
    end
end

%--------------------------------------------------------------
% Initializations
%--------------------------------------------------------------

initialStateMean_sim = [180 50 0]';
if ~exist('initialStateMean','var') || isempty(initialStateMean)
   initialStateMean = initialStateMean_sim; % variance of noise proportional to alphas
end
% Motion noise (in odometry space, see Table 5.5, p.134 in book).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
alphas_sim = [0.05 0.001 0.05 0.01].^2; % variance of noise proportional to alphas
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~exist('alphas','var') || isempty(alphas)
   alphas = alphas_sim; % variance of noise proportional to alphas
end

beta_sim = deg2rad(20);
% Standard deviation of Gaussian sensor noise (independent of distance)
if ~exist('beta','var') || isempty(beta)
    beta = deg2rad(20);
end
% Step size between filter updates, can be less than 1.
deltaT=0.1;

persistent data numSteps;
if isempty(stepsOrData) % use dataset from last time
    if isempty(data)
        numSteps = 100;
        data = generateScript(initialStateMean_sim, numSteps, alphas_sim, beta_sim, deltaT);
    end
elseif isscalar(stepsOrData)
    % Generate a dataset of motion and sensor info consistent with
    % noise models.
    numSteps = stepsOrData;
    data = generateScript(initialStateMean_sim, numSteps, alphas_sim, beta_sim, deltaT);
else
    % use a user supplied dataset from a previous run
    data = stepsOrData;
    numSteps = size(data, 1);
    global FIELDINFO;
    FIELDINFO = getfieldinfo;
end


% TODO: provide proper initialization for your filters here
% You can set the initial mean and variance of the EKF to the true mean and
% some uncertainty.

% EKF Initializations 

Q = beta^2;
ekf_mu = initialStateMean;
ekf_Sigma = diag([1,1,0.1]);
%[ 
    %1 0.01 0.01;
    %0.01 1 0.01;
    %0.01 0.01 0.1];
ekf_Q = Q;

% UKF Initializations 
ukf_mu = initialStateMean;
ukf_Sigma = diag([1,1,0.1]);
ukf_Q = Q;

% PF Initialization
if ~exist('pf_numSamples','var') || isempty(pf_numSamples)
    pf_numSamples = 200;
end
pf_weight = zeros(1,pf_numSamples);
pf_mu = initialStateMean;
pf_Sigma = diag([20,20,0.1]);
pf_Q = Q;


pf_samples = pf_Sigma*randn(3,pf_numSamples) + repmat(pf_mu,1,pf_numSamples);
%pf_samples = 200*randn(3,pf_numSamples);
for i = 1:pf_numSamples
    pf_samples(3,i) = minimizedAngle(pf_samples(3,i));
end

M=0; %Passing alphas instead and composing M inside function

% Call ekfUpdate, ukfUpdate and pfUpdate in every iteration of this loop.
% You might consider putting in a switch yard so you can select which
% algorithm does the update
results = [];
if ~exist('filter_no','var') || isempty(filter_no)
    filter_no = [ 1 ];
end
plots_enabled = false; %Enable real time error plots. Significantly slows down program.
final_plots_enabled = false; %Enable real time error plots. Significantly slows down program.


ekf_history = [];
ukf_history = [];
pf_history = [];

kidnapped = false; %Set to true to test kidnapped robot situation


for t = 1:numSteps

      
    if kidnapped==false && t==50
         data = generateScript(initialStateMean, numSteps, alphas, beta, deltaT);
         data(t+1:numSteps,:) = data(1:numSteps-t,:);
         kidnapped = true;
    end
    
    
    %=================================================
    % data available to your filter at this time step
    %=================================================
    motionCommand = data(t,3:5)'; % [drot1, dtrans, drot2]' noisefree control command
    observation = data(t,1:2)';   % [bearing, landmark_id]' noisy observation

    %=================================================
    % data *not* available to your filter, i.e., known
    % only by the simulator, useful for making error plots
    %=================================================
    % actual position (i.e., ground truth)
    x = data(t,8);
    y = data(t,9);
    theta = data(t,10);

    % noisefree observation
    noisefreeBearing = data(t, 6);

    %=================================================
    % graphics
    %=================================================
    figure(GLOBAL_FIGURE); clf; hold on; plotfield(observation(2));

    % draw actual path and path that would result if there was no noise in
    % executing the motion command
    plot([initialStateMean(1) data(1,8)], [initialStateMean(2) data(1,9)], 'Color', ACTUAL_PATH_COL);
    plot([initialStateMean(1) data(1,11)], [initialStateMean(2) data(1,12)], 'Color', NOISEFREE_PATH_COL);

    % draw actual path (i.e., ground truth)
    plot(data(1:t,8), data(1:t,9), 'Color', ACTUAL_PATH_COL);
    plotrobot( x, y, theta, 'black', 1, 'cyan');

    % draw noise free motion command path
    plot(data(1:t,11), data(1:t,12), 'Color', NOISEFREE_PATH_COL);
    plot(data(t,11), data(t,12), '*', 'Color', NOISEFREE_PATH_COL);

    % indicate observed angle relative to actual position
    plot([x x+cos(theta+observation(1))*100], [y y+sin(theta+observation(1))*100], 'Color', OBSERVED_BEARING_COLOR);

    % indicate ideal noise-free angle relative to actual position
    plot([x x+cos(theta+noisefreeBearing)*100], [y y+sin(theta+noisefreeBearing)*100], 'Color', NOISEFREE_BEARING_COLOR);

    %=================================================
    %TODO: update your filter here based upon the
    %      motionCommand and observation
    %=================================================
    if max(filter_no == 1) %Call EKF
        [ekf_mu, ekf_Sigma, ekf_predMu, ekf_predSigma, ekf_zhat, ekf_G, ekf_R, ekf_H, ekf_K] = ...
            ekfUpdate(ekf_mu, ekf_Sigma, motionCommand, deltaT, observation(1), ekf_Q, observation(2), alphas);
        
        ekf_history = [ekf_history, [...
            ekf_mu - [x; y; theta]; ...
            ekf_Sigma(1,1); ...
            ekf_Sigma(2,2); ...
            ekf_Sigma(3,3); ]...
            ];
        ekf_history(3,t) = minimizedAngle(ekf_history(3,t));
    end
    
    if max(filter_no == 2) %Call UKF
        [ukf_mu, ukf_Sigma, ukf_predMu, ukf_predSigma, ukf_zhat, ukf_G, ukf_R, ukf_H, ukf_K] = ...
            ukfUpdate(ukf_mu, ukf_Sigma, motionCommand, deltaT, observation(1), ukf_Q, observation(2), alphas);
        
        ukf_history = [ukf_history, [...
            ukf_mu - [x; y; theta]; ...
            ukf_Sigma(1,1); ...
            ukf_Sigma(2,2); ...
            ukf_Sigma(3,3); ]...
            ];
        ukf_history(3,t) = minimizedAngle(ukf_history(3,t));
        
    end
    if max(filter_no == 3) %Call PF
        [pf_samples, pf_weight, pf_mu, pf_Sigma, pf_predState, pf_predMu, pf_predSigma, pf_zHat] = ...
            pfUpdate( pf_samples, pf_weight, pf_numSamples, motionCommand, deltaT, M, observation(1), pf_Q, observation(2), alphas);
        pf_history = [pf_history, [...
            pf_mu - [x; y; theta]; ...
            pf_Sigma(1,1); ...
            pf_Sigma(2,2); ...
            pf_Sigma(3,3); ]...
            ];
        pf_history(3,t) = minimizedAngle(pf_history(3,t));
    end
    %=================================================
    %TODO: plot and evaluate filter results here
    %=================================================
    if plots_enabled 

        figure(2)
        clf
        title('Pose Error - X') 

        figure(3)
        clf
        title('Pose Error - Y')

        figure(4)
        clf
        title('Pose Error - Theta')
    end    
    figure(1)
    
    if max(filter_no == 1) %Call EKF
        plotcircle(ekf_mu(1:2), 10, 100, 'y', true, 'y');
        pekf1 = plotcov2d(ekf_mu(1), ekf_mu(2), ekf_Sigma(1:2,1:2), 'y', true, 'y', 0.5, 3);
        plotcircle(ekf_predMu(1:2), 10, 100, 'm', true, 'm');
        pekf2 = plotcov2d(ekf_predMu(1), ekf_predMu(2), ekf_predSigma(1:2,1:2), 'm', true, 'm', 0.5, 3);
        legend([pekf1, pekf2 ], 'EKF-', 'EKF+')
        
        if plots_enabled 

            figure(2)
            hold on
            %errorbar(ekf_history(1,:), 3*sqrt(ekf_history(4,:)), 'r');
            errorbar(repmat(0,1,t), 3*sqrt(ekf_history(4,:)), 'r');
            plot(ekf_history(1,:), 'b');

            figure(3)
            hold on 
            errorbar(repmat(0,1,t), 3*sqrt(ekf_history(5,:)), 'r');
            plot(ekf_history(2,:), 'b');

            figure(4)
            hold on 
            errorbar(repmat(0,1,t), 3*sqrt(ekf_history(6,:)), 'r');
            plot(ekf_history(3,:), 'b');
        end
        figure(1)
        
    end
    if max(filter_no == 2) %Call UKF
        plotcircle(ukf_mu(1:2), 10, 100, 'c', true, 'c');
        pukf1 = plotcov2d(ukf_mu(1), ukf_mu(2), ukf_Sigma(1:2,1:2), 'c', true, 'c', 0.5, 3);
        plotcircle(ukf_predMu(1:2), 10, 100, 'r', true, 'r');
        pukf2 = plotcov2d(ukf_predMu(1), ukf_predMu(2), ukf_predSigma(1:2,1:2), 'r', true, 'r', 0.5, 3);
        legend([pukf1, pukf2 ], 'UKF-', 'UKF+')
        
        if plots_enabled 

            figure(2)
            hold on 
            errorbar(zeros(1,t), 3*sqrt(ukf_history(4,:)), 'r');
            plot(ukf_history(1,:), 'b');

            figure(3)
            hold on 
            errorbar(zeros(1,t), 3*sqrt(ukf_history(5,:)), 'r');
            plot(ukf_history(2,:), 'b');

            figure(4)
            hold on 
            errorbar(zeros(1,t), 3*sqrt(ukf_history(6,:)), 'r');
            plot(ukf_history(3,:), 'b');

        end
        figure(1)

        
    end
    if max(filter_no == 3) %Call PF
       plotSamples(pf_samples);
       
       if plots_enabled 

           figure(2)
           clf
           hold on 
           errorbar(repmat(0,1,t), 3*sqrt(pf_history(4,:)), 'r');
           plot(pf_history(1,:), 'b');

           figure(3)
           hold on 
           errorbar(repmat(0,1,t), 3*sqrt(pf_history(5,:)), 'r');
           plot(pf_history(2,:), 'b');

           figure(4)
           hold on 
           errorbar(repmat(0,1,t), 3*sqrt(pf_history(6,:)), 'r');
           plot(pf_history(3,:), 'b');
       end
        
       figure(1)
 
    end

    drawnow;
    if pauseLen == inf
        pause;
    elseif pauseLen > 0
        pause(pauseLen);
    end

    if makeVideo
        F = getframe(gcf);
        switch votype
          case 'avifile'
            vo = addframe(vo, F);
          case 'VideoWriter'
            writeVideo(vo, F);
          otherwise
            error('unrecognized votype');
        end
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate final plots

if final_plots_enabled
    figure(100)
    clf
    hold on 
    n = length(filter_no);
    i=1;

    if max(filter_no == 1) %Call EKF
        subplot(n,3,i)
        hold on
        errorbar(zeros(1,t), 3*sqrt(ekf_history(4,:)), 'r');
        plot(ekf_history(1,:), 'b');
        title('EKF X Error')

        subplot(n,3,i+1)
        hold on
        errorbar(zeros(1,t), 3*sqrt(ekf_history(5,:)), 'r');
        plot(ekf_history(2,:), 'b');
        title('EKF Y Error')

        subplot(n,3,i+2)
        hold on
        errorbar(zeros(1,t), 3*sqrt(ekf_history(6,:)), 'r');
        plot(ekf_history(3,:), 'b');
        title('EKF Angle Error')

        i = i+3;
    end

    if max(filter_no == 2) %Call EKF
        subplot(n,3,i)
        hold on
        errorbar(zeros(1,t), 3*sqrt(ukf_history(4,:)), 'r');
        plot(ukf_history(1,:), 'b');
        title('UKF X Error')

        subplot(n,3,i+1)
        hold on
        errorbar(zeros(1,t), 3*sqrt(ukf_history(5,:)), 'r');
        plot(ukf_history(2,:), 'b');
        title('UKF Y Error')


        subplot(n,3,i+2)
        hold on
        errorbar(zeros(1,t), 3*sqrt(ukf_history(6,:)), 'r');
        plot(ukf_history(3,:), 'b');
        title('UKF Angle Error')

        i = i+3;
    end
    
    if max(filter_no == 3) %Call EKF
        subplot(n,3,i)
        hold on
        errorbar(zeros(1,t), 3*sqrt(pf_history(4,:)), 'r');
        plot(pf_history(1,:), 'b');
        title('PF X Error')

        subplot(n,3,i+1)
        hold on
        errorbar(zeros(1,t), 3*sqrt(pf_history(5,:)), 'r');
        plot(pf_history(2,:), 'b');
        title('PF Y Error')

        subplot(n,3,i+2)
        hold on
        errorbar(zeros(1,t), 3*sqrt(pf_history(6,:)), 'r');
        plot(pf_history(3,:), 'b');
        title('PF Angle Error')

        i = i+3;
    end
end

%filename = strcat('../../../figures/f', num2str(filter_no), 'beta', num2str(beta));
%print(filename,'-dpng');
if max(filter_no == 1)
    results = [results, ekf_history];
end
if max(filter_no == 2)
    results = [results, ukf_history];
end
if max(filter_no == 3)
    results = [results, pf_history];
end


if nargout >= 1
    varargout{1} = data;
end
if nargout >= 2
    varargout{2} = results;
end

if makeVideo
    fprintf('Writing video...');
    switch votype
      case 'avifile'
        vo = close(vo);
      case 'VideoWriter'
        close(vo);
      otherwise
        error('unrecognized votype');
    end
    fprintf('done\n');
end