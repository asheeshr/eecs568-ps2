function [samples, weight, mu, Sigma, predState, predMu, predSigma, zHat] = pfUpdate( ...
    samples, weight, numSamples, u, deltaT, M, z, Q, markerId, ...
    alphas)

% NOTE: The header is not set in stone.  You may change it if you like
global FIELDINFO;
landmark_x = FIELDINFO.MARKER_X_POS(markerId);
landmark_y = FIELDINFO.MARKER_Y_POS(markerId);

stateDim=3;
motionDim=3;
observationDim=2;

% ----------------------------------------------------------------
% Prediction step
% ----------------------------------------------------------------
%disp('.......................................')

x_t = zeros(3,numSamples);
for i=1:numSamples
    x_t(:,i) = sampleOdometry(u,samples(:,i),alphas); %Handles angle rollover
end

% Compute mean and variance of estimate. Not really needed for inference.
[predMu, predSigma] = meanAndVariance(samples, numSamples);

predState = x_t;
samples = x_t;

% ----------------------------------------------------------------
% Correction step
% ----------------------------------------------------------------

%z_t = zeros(numSamples,1);
disty = repmat(landmark_y,1,numSamples) - x_t(2,:);
distx = repmat(landmark_x,1,numSamples) - x_t(1,:);
z_t = atan2(disty, distx) - x_t(3,:);

for i=1:numSamples
    z_t(i) = minimizedAngle(z_t(i));
    %weight(i) = (abs(z-z_t(i)+normrnd(0,sqrt(Q))))^(-1);     
    weight(i) = normpdf(minimizedAngle(z-z_t(i)), 0, sqrt(Q));
end

weight = weight/norm(weight,1);
zHat = z_t;

[samples, newWeight] = resample(samples, weight);
[mu, Sigma] = meanAndVariance(samples, numSamples);