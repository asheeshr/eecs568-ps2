function [newSamples, newWeight] = resample(samples, weight)

weight = weight/norm(weight,1);
cumulative_weights = zeros(1,length(samples));
cumulative_weights(1) = weight(1);
for i=2:length(samples)
    cumulative_weights(i) = cumulative_weights(i-1) + weight(i);
end
cumulative_weights;
%cumulative_weights = cumsum(weight)

newSamples = zeros(3,length(samples));

%Stratified Resampling

u_k_bar = rand(1,length(samples));
u_k = u_k_bar/length(samples);
for i=1:length(samples)
    u_k(i) = (i-1)/length(samples) + u_k(i);
    index = find(u_k(i)<=cumulative_weights, 1 );
    if isempty(index)
        index = find(u_k(i)>cumulative_weights, 1 );
        if isempty(index)
            disp('ERROR')
            disp(index)
        end
    end    
    newSamples(:,i) = samples(:,index);
end

newWeight = zeros(1,length(samples));