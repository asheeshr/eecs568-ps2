%Iterating over sensor noise

filter_no = [1 2 3];
ekf_localization_error = [];
ukf_localization_error = [];
pf_localization_error = [];
beta = deg2rad([5:5:20, 20:10:40]);
iterations = 50;
trials = 5;
counter=0;
for i=beta
    counter = counter+1;
    disp('Running for beta = ')
    disp(i)
    trial_data = zeros(3,trials);
    for j=1:trials
        [data, results] = runfunc(iterations,10e-6,false,i,filter_no);
        trial_data(1,j) = mean(sqrt(results(1,1:iterations).^2+results(2,1:iterations).^2));
        trial_data(2,j) = mean(sqrt(results(1,iterations+1:2*iterations).^2+results(2,iterations+1:2*iterations).^2));
        trial_data(3,j) = mean(sqrt(results(1,2*iterations+1:3*iterations).^2+results(2,2*iterations+1:3*iterations).^2));
    end
    ekf_localization_error = [ekf_localization_error; mean(trial_data(1,:)) , 3*std(trial_data(1,:))];
    ukf_localization_error = [ukf_localization_error; mean(trial_data(2,:)) , 3*std(trial_data(2,:))];
    pf_localization_error = [pf_localization_error;   mean(trial_data(3,:)) , 3*std(trial_data(3,:))];    
    figure(200)
    clf
    hold on
    % plot(beta,ekf_localization_error,beta,ukf_localization_error,beta,pf_localization_error)
    errorbar(beta(1:counter),ekf_localization_error(:,1),ekf_localization_error(:,2))
    errorbar(beta(1:counter),ukf_localization_error(:,1),ukf_localization_error(:,2))
    errorbar(beta(1:counter),pf_localization_error(:,1),pf_localization_error(:,2))

    title('Effect of Sensor Noise on Filters')
    legend('EKF','UKF','PF')
    xlabel('Beta (rad)')
    ylabel('Avg Localization Error')

end


filename = strcat('../../../figures/task2c','beta');
print(filename,'-dpng');